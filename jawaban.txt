Berlatih SQL

Soal 1 Membuat Database

create database myshop;

------------------------------------------------------------
Soal 2 Membuat Table di Dalam Database

Tabel users :
create table users (
    -> id int(15) PRIMARY KEY auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

Tabel categories : 
create table categories (id int(15) PRIMARY KEY auto_increment, name varchar(255));

Tabel items :
 create table items (
    -> id int(15) PRIMARY KEY auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> foreign key(category_id) references categories(id)
    -> );

----------------------------------------------------
Soal 3 Memasukkan Data pada Table

Tabel users :
-  insert into users (name , email, password) values("John Doe", "john@doe.com", "john123");
-  insert into users (name, email, password) values("Jane Doe", "jane@doe.com", "jenita123");

Tabel categories :
 INSERT INTO categories (name) VALUES ("gadget"),("cloth"),("men"),("women"),("branded");

Tabel items : 
>  INSERT INTO items (name,description,price,stock,category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000,100, 1);
>  insert into items (name, description, price, stock, category_id) values ("Uniklooh", "baju keren dari brand ternama", 500000,50,2),("IMHO Watch","jam tangan anak yang jujur banget", 2000000,10,1);

------------------------------------------------------
Soal 4 Mengambil Data dari Database
 

a. Mengambil data users :
SELECT id, name, email FROM users;

b. Mengambil data items :
    > select * from items where price > 1000000;
    
    > SELECT * FROM items WHERE name LIKE '%sang%'; 
       SELECT * FROM items WHERE name LIKE '%uniklo%';

c. Menampilkan data items join dengan kategori

select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories on items.category_id = categories.id;

----------------------------------------------------- 
Soal 5 Mengubah Data dari Database 

UPDATE items set price=2500000 where name="Sumsang b50"; 